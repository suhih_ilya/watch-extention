//
//  InterfaceController.swift
//  BitWatch WatchKit Extension
//
//  Created by admin on 26/12/14.
//  Copyright (c) 2014 Razeware LLC. All rights reserved.
//

import WatchKit
import Foundation
import BitWatchKit

class InterfaceController: WKInterfaceController {


    @IBOutlet weak var priceLabel: WKInterfaceLabel!
    @IBOutlet weak var refrechButton: WKInterfaceButton!
    
    @IBOutlet weak var lastUpdateLabel: WKInterfaceLabel!
    

        
        private func updatePrice(price: NSNumber) {
            self.priceLabel.setText( Tracker.priceFormatter.stringFromNumber(price))
            
            self.lastUpdateLabel.setText("Обновлено \(Tracker.dateFormatter.stringFromDate(NSDate()))")
            
        }
        
        
    
    
    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
        
        lastUpdateLabel.setText("");
        
        let tracker = Tracker()
        let originalPrice = tracker.cachedPrice()
        
        
        updatePrice(originalPrice)
        tracker.requestPrice { (price, error) -> () in
            if error? == nil {
                self.updatePrice(price!)
            }
        }
        

        
    }
    
    



    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
      

    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    @IBAction func refresh() {
        
    
        
        Tracker().requestPrice { (price, error) -> () in
            if error? == nil {
                self.updatePrice(price!)
            }
            else
            {
                self.lastUpdateLabel.setText("Ошибка")
            }
            
        }
        
        
    }
}
